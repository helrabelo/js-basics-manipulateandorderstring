// Your task is to sort a given string

// Each word in the string will contain a single number. 

// The number is the position the word should have in the SpeechRecognitionResult

// Note: Numbers can be from 1 to 9. So 1 will be the first word (not 0).a

// If the input string is empty, return an empty string. The words in the input String will contain valid consecutive numbers. 

// Also, remove the numbers when printing the ordened string


// Metodologia:

// 1- Quebrar toda a String e armazenar cada palavra em um vetor 

// 2- Remover o número de cada palavra

// 3- Ordenar o vetor de acordo com o número 


// 4- Printar a palavra


//BREAK STRING AND RETURN ARRAY OF UNORDERED WORDS W/ DIGIT INSIDE
function breakString(fullString){
    return fullString.split(' ');
}

//SWAP ELEMENTS POSITION WITHIN ARRAY
function swapElements(arrayOfElements, firstElement, secondElement){
    let temp = arrayOfElements[secondElement];
    arrayOfElements[secondElement] = arrayOfElements[firstElement];
    arrayOfElements[firstElement] = temp;
}


//GET NUMBER FROM STRING AND RETURN IT
function getIntFromString(stringWithInt){
    return stringWithInt.match(/[0-9]/)[0];
}

//ORDER ARRAY ACCORDING TO INT 
function orderString(unorderedString){
    
    let i = 0;

    while (i < unorderedString.length){
        if (getIntFromString(unorderedString[i]) != i+1 ){
            swapElements(unorderedString, i, (getIntFromString(unorderedString[i]) - 1));
        } else {
            unorderedString[i] = unorderedString[i].replace(/[0-9]/, "");
            i++;
        }
    }

    let orderString = unorderedString.join(", ");

    return orderString;
}

let testString = "T2wo T3hree F5ive O1ne S6ix F4our N9ine S7even E8ight";
testString = orderString(breakString(testString));
console.log(testString);
